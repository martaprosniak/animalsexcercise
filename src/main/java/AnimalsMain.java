import models.*;

import java.util.Scanner;

public class AnimalsMain {

    /**
     * Main application method
     */
    public static void main(String[] args) {

        Animal[] animals = new Animal[10];

        while (true) {

            System.out.println("Choose what to do: \n" +
                    "add \n" +
                    "print \n" +
                    "print_all \n" +
                    "speak \n" +
                    "stroke \n" +
                    "quit");
            /*Use Scanner class to read user input
            Creates new Scanner object*/
            Scanner inputScanner = new Scanner(System.in);

            //Next user command
            String command = inputScanner.next();

            if ("add".equals(command)) {
                //Create local variables for animal name and age
                System.out.print("Enter animal index: ");
                int animalIndex = inputScanner.nextInt();
                System.out.print("Enter animal species: ");
                String species = inputScanner.next();
                System.out.print("Enter animal race: ");
                String race = inputScanner.next();
                System.out.print("Enter animal name: ");
                String name = inputScanner.next();
                System.out.print("Enter animal age: ");
                int age = inputScanner.nextInt();

                Animal animal = null;
                //Create new animal
                if ("dog".equals(species)){
                    if ("labrador".equals(race)){
                        animal = new Labrador(name, age);
                    } else animal = new Dog(name, age);
                } else if ("cat".equals(species)){
                    if ("pers".equals(race)){
                        animal = new Pers(name, age);
                    } else animal = new Cat(name, age);
                } else if ("wolf".equals(species)){
                    animal = new Wolf(name, age);
                } else System.out.println("Unknown species!");

                //Assign local variables to more global ones
                animals[animalIndex] = animal;

                //Print info about animal
                System.out.println("Created animal: " + animal.getName() +
                        " " + animal.getAge() + "\n");

            }   else if ("print".equals(command)){
                System.out.println("Enter index of animal to be printed");
                int animalIndex = inputScanner.nextInt();
                System.out.println("Name: " + animals[animalIndex].getName() +
                        ", age: " + animals[animalIndex].getAge());
            } else if ("print_all".equals(command)){
                for (Animal a : animals){
                    if (a != null) {
                        System.out.println("Name: " + a.getName() +
                                ", age: " + a.getAge());
                    }
                }
            } else if ("speak".equals(command)){ //depends of animal species
                for (Animal a : animals){
                    if (a instanceof Dog){
                        if (a instanceof Labrador){
                            System.out.print(a.getName() + ": ");
                            ((Labrador) a).hau();
                        } else {
                            System.out.print(a.getName() + ": ");
                            ((Dog) a).hau();
                        }
                    } else if (a instanceof Cat){
                        if (a instanceof Pers){
                            System.out.print(a.getName() + ": ");
                            ((Pers) a).miau();
                        } else {
                            System.out.print(a.getName() + ": ");
                            ((Cat) a).miau();
                        }
                    } else if (a instanceof Wolf){
                        System.out.print(a.getName() + ": ");
                        ((Wolf) a).woof();
                    }
                }
            } else if ("stroke".equals(command)){ //depends of animal species
                for (Animal a : animals){
                    if (a instanceof Dog){
                        if (a instanceof Labrador){
                            System.out.print(a.getName() + ": ");
                            ((Labrador) a).stroke();
                        } else {
                            System.out.print(a.getName() + ": ");
                            ((Dog) a).stroke();
                        }
                    } else if (a instanceof Cat){
                        if (a instanceof Pers){
                            System.out.print(a.getName() + ": ");
                            ((Pers) a).stroke();
                        } else {
                            System.out.print(a.getName() + ": ");
                            ((Cat) a).stroke();
                        }
                    }
                }
            } else if ("quit".equals(command)){
                break;
            }
        }

    }
}

