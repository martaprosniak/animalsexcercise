package models;
/**
 * Animal representation
 * @author Marta
 */
public class Animal {
    /**
     *Animal name
     */
    private String name;
    /**
     * Animal age
     */
    private int age;

    /**
     * Animal constructor without parameters
     */
    public  Animal(){
    }

    /**
     * Animal constructor without parameters
     * @param name New animal name
     * @param age New animal age
     */
    public Animal(String name, int age){
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
