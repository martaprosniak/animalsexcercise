package models;

public class Cat extends Animal implements Pet {

    public void miau () {
        System.out.println("Miaaauu!");
    }

    public void stroke() {
        miau();
    }

    public Cat(String name, int age) {
        super(name, age);
    }
}
