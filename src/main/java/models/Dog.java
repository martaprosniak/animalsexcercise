package models;

public class Dog extends Animal implements Pet {

    public void hau (){
        System.out.println("Hau hau!");
    }

    public void stroke() {
        hau();
    }

    public Dog (String name, int age){
        super(name, age);
    }
}
