package models;

public class Labrador extends Dog {

    @Override
    public void hau() {
        System.out.println("Wow! Gimmie something to eat! Wow! Wow!");
    }

    public Labrador (String name, int age){
        super(name, age);
    }
}
