package models;

public class Pers extends Cat {

    @Override
    public void miau() {
        System.out.println("Ssss! Ssss!");
    }

    public Pers(String name, int age) {
        super(name, age);
    }
}
