package models;

public class Wolf extends Animal {

    public void woof (){
        System.out.println("Auuuuuuuuu!");
    }

    public Wolf(String name, int age) {
        super(name, age);
    }
}
